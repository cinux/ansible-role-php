"""Role testing files using testinfra."""


def test_php_installed(host):
    """Validate php is installed"""

    assert host.package('php').is_installed
    assert host.package('php').version.startswith("2:7.4")
