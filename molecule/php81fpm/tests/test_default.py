"""Role testing files using testinfra."""


def test_php_installed(host):
    """Validate php is installed"""

    assert host.package('php').is_installed
    assert host.package('php').version.startswith("2:8.1")


def test_php_fpm_installed(host):
    """Validate php-fpm is installed"""

    assert host.package('php8.1-fpm').is_installed
    assert host.package('php8.1-fpm').version.startswith("8.1")
