"""Role testing files using testinfra."""


def test_php_installed(host):
    """Validate php is installed"""

    assert host.package('php').is_installed
    assert host.package('php').version.startswith("2:8.1")


def test_php_fpm_installed(host):
    """Validate php-fpm is installed"""

    assert not host.package('php8.1-fpm').is_installed


def test_php_fpm_pool_www(host):
    """Validate www.conf exists"""

    file = host.file("/etc/php/8.1/fpm/pool.d/")
    assert not file.exists
