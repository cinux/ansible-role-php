# php

Installs and configure PHP.

## Requirements

[Ondrej Sury PHP repository](https://packages.sury.org/) is needed to install php 8.1.

## Role Variables

none

## Dependencies

none

## Example Playbook

```yaml
    - hosts: servers
      roles:
         - { role: cinux.php }
```

## License

MIT
